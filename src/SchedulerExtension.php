<?php

declare(strict_types=1);

namespace ElektroPotkan\Scheduler\DI;

use Contributte\DI\Helper\ExtensionDefinitionsHelper;
use InvalidArgumentException;
use Nette\DI\CompilerExtension;
use Nette\DI\Definitions\Definition;
use Nette\DI\Definitions\ServiceDefinition;
use Nette\DI\Definitions\Statement;
use Nette\DI\MissingServiceException;
use Nette\Schema\Expect;
use Nette\Schema\Schema;
use stdClass;
use Tracy;

use ElektroPotkan\Scheduler\Console\Commands;
use ElektroPotkan\Scheduler\ILogger;
use ElektroPotkan\Scheduler\Jobs\Callback as CallbackJob;
use ElektroPotkan\Scheduler\Schedulers;
use ElektroPotkan\Scheduler\TracyLogger;


/**
 * @property-read stdClass $config
 */
class SchedulerExtension extends CompilerExtension {
	public function getConfigSchema(): Schema {
		$jobs = Expect::arrayOf(
			Expect::anyOf(
				Expect::string(),
				Expect::array(),
				Expect::type(Statement::class)
			)
		);
		
		return Expect::anyOf(
			Expect::structure([
				'path' => Expect::string()->required(),
				'lastX' => Expect::anyOf('check', 'run', false)->firstIsDefault(),
				'jobs' => $jobs,
			]),
			Expect::structure([
				'jobs' => $jobs,
			])
		);
	} // getConfigSchema
	
	public function loadConfiguration(): void {
		$builder = $this->getContainerBuilder();
		$config = $this->config;
		$definitionHelper = new ExtensionDefinitionsHelper($this->compiler);
		
		// scheduler
		$schedulerDefinition = new ServiceDefinition;
		$builder->addDefinition($this->prefix('scheduler'), $schedulerDefinition);
		
		if(isset($config->path)){
			$class = Schedulers\Locking::class;
			
			if($config->lastX === 'check'){
				$class = Schedulers\LastCheck::class;
			}
			elseif($config->lastX === 'run'){
				$class = Schedulers\LastRun::class;
			};
			
			$schedulerDefinition->setFactory($class, [$config->path]);
		}
		else {
			$schedulerDefinition->setFactory(Schedulers\Simple::class);
		};
		
		// commands
		$cmds = [
			// @phpstan-ignore-next-line
			'run' => Commands\Run::class,
			// @phpstan-ignore-next-line
			'forceRun' => Commands\ForceRun::class,
			// @phpstan-ignore-next-line
			'list' => Commands\ListCommand::class,
			// @phpstan-ignore-next-line
			'help' => Commands\Help::class,
		];
		
		foreach($cmds as $name => $class){
			if(class_exists($class)){
				$def = (new ServiceDefinition)
					->setFactory($class)
					->setAutowired(false);
				$builder->addDefinition($this->prefix("console.cmd.$name"), $def);
			};
		};
		
		// logging
		if(interface_exists(Tracy\ILogger::class)){
			$def = (new ServiceDefinition)
				->setFactory(TracyLogger::class)
				->setAutowired(false);
			$builder->addDefinition($this->prefix('logger'), $def);
		};
		
		// jobs
		foreach($config->jobs as $jobName => $jobConfig){
			if(is_array($jobConfig) && (isset($jobConfig['cron']) || isset($jobConfig['callback']))){
				if(!isset($jobConfig['cron'], $jobConfig['callback'])){
					throw new InvalidArgumentException(sprintf('Both options "callback" and "cron" of %s > jobs > %s must be configured', $this->name, $jobName));
				};
				
				$jobDefinition = new Statement(CallbackJob::class, [$jobConfig['cron'], $jobConfig['callback']]);
			}
			else {
				$jobPrefix = $this->prefix('jobs.' . $jobName);
				$jobDefinition = $definitionHelper->getDefinitionFromConfig($jobConfig, $jobPrefix);
				if($jobDefinition instanceof Definition){
					$jobDefinition->setAutowired(false);
				};
			};
			
			$schedulerDefinition->addSetup('add', [$jobDefinition, $jobName]);
		}
	} // loadConfiguration
	
	public function beforeCompile(): void {
		$builder = $this->getContainerBuilder();
		
		$loggerDef = null;
		
		try {
			$loggerDef = $builder->getDefinitionByType(ILogger::class);
		}
		catch(MissingServiceException $e){};
		
		if($loggerDef === null){
			try {
				$loggerDef = $builder->getDefinition($this->prefix('logger'));
				$loggerDef->setAutowired(true);
			}
			catch(MissingServiceException $e){};
		};
		
		if($loggerDef !== null){
			$defs = $builder->findByType(Schedulers\Simple::class);
			foreach($defs as $def){
				if($def instanceof ServiceDefinition){
					$def->addSetup('setLogger', [$loggerDef]);
				};
			};
		};
	} // beforeCompile
} // class SchedulerExtension
