<?php

declare(strict_types=1);

namespace Tests\Cases\SchedulerExtension;

require __DIR__ . '/../../bootstrap.php';

use Nette\DI\Compiler;
use Nette\DI\Container;
use Nette\DI\ContainerLoader;
use Tester;
use Tester\Assert;
use Tester\FileMock;

use ElektroPotkan\Scheduler\DI\SchedulerExtension;
use ElektroPotkan\Scheduler\IScheduler;
use ElektroPotkan\Scheduler\Schedulers;


class Test extends Tester\TestCase {
	public function getKey(): array {
		return [
			getmypid(),
			rand(),
			lcg_value(),
		];
	} // getKey
	
	public function testParse(): void {
		$loader = new ContainerLoader(TESTS_DIR_TEMP, true);
		
		$containerClass = $loader->load(
			function(Compiler $compiler): void {
				$compiler->addExtension('scheduler', new SchedulerExtension());
				$compiler->loadConfig(
					FileMock::create("
						services:
							callbackJob: Tests\Mocks\CallbackJob
							scheduledJob: Tests\Mocks\CustomJob
						
						scheduler:
							jobs:
								- {cron: '* * * * *', callback: Tests\Mocks\CallbackJob::foo}
								- {cron: '* * * * *', callback: [@callbackJob, bar]}
								- Tests\Mocks\CustomJob
								- @scheduledJob
						",
						'neon'
					)
				);
			},
			$this->getKey()
		);
		
		/** @var Container $container */
		$container = new $containerClass;
		
		$scheduler = $container->getByType(IScheduler::class);
		Assert::type(IScheduler::class, $scheduler);
		Assert::count(4, $scheduler->getAll());
	} // testParse
	
	public static function genArgsSelectScheduler(): array {
		return [
			[
				"
				scheduler:
				",
				Schedulers\Simple::class
			],
			[
				"
				scheduler:
					path: /tmp
					lastX: no
				",
				Schedulers\Locking::class
			],
			[
				"
				scheduler:
					path: /tmp
				",
				Schedulers\LastCheck::class
			],
			[
				"
				scheduler:
					path: /tmp
					lastX: check
				",
				Schedulers\LastCheck::class
			],
			[
				"
				scheduler:
					path: /tmp
					lastX: run
				",
				Schedulers\LastRun::class
			],
		];
	} // genArgsInvalidUInt
	
	/**
	 * @dataProvider genArgsSelectScheduler
	 */
	public function testSelectScheduler($neon, $class): void {
		$loader = new ContainerLoader(TESTS_DIR_TEMP, true);
		
		$containerClass = $loader->load(
			function(Compiler $compiler) use($neon): void {
				$compiler->addExtension('scheduler', new SchedulerExtension());
				$compiler->loadConfig(
					FileMock::create($neon, 'neon')
				);
			},
			$this->getKey()
		);
		
		/** @var Container $container */
		$container = new $containerClass;
		
		$scheduler = $container->getByType(IScheduler::class);
		Assert::type($class, $scheduler);
	} // testSelectScheduler
} // class Test


(new Test)->run();
