<?php

declare(strict_types=1);


/* Load Composer-maintained packages */
require_once dirname(__DIR__).'/vendor/autoload.php';


/* Additional directories */
define('TESTS_DIR', '/tmp/php-packages-tests/elektro-potkan/scheduler-di');
@mkdir(TESTS_DIR);

define('TESTS_DIR_TEMP', TESTS_DIR.'/temp');
@mkdir(TESTS_DIR_TEMP);


/* Tester setup */
Tester\Environment::setup();
