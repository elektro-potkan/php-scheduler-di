<?php

declare(strict_types=1);

namespace Tests\Mocks;


final class CallbackJob {
	public static function foo(): void {}
	
	public function bar(): void {}
} // class CallbackJob
