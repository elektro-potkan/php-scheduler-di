<?php

declare(strict_types=1);

namespace Tests\Mocks;

use DateTimeInterface;

use ElektroPotkan\Scheduler\IJob;


final class CustomJob implements IJob {
	public function isDue(
		DateTimeInterface $now,
		?DateTimeInterface $lastCheck = null,
		?DateTimeInterface $lastRun = null
	): bool {
		return true;
	} // isDue
	
	public function run(): void	{}
} // class CustomJob
