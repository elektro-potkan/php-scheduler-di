Scheduler - Nette DI integration
================================

Part of heavily modified fork of [contributte/scheduler](https://github.com/contributte/scheduler).

Simplifies scheduler setup in projects using Nette DI container.


Usage
-----

### Example
```neon
extensions:
	scheduler: ElektroPotkan\Scheduler\DI\SchedulerExtension

services:
	stats: App\Model\Stats
	scheduledJob: App\Model\ScheduledJob
	- App\Model\OtherScheduledJob

scheduler:
	path: '%tempDir%/scheduler'
	jobs:
		# stats must be registered as a service and have method calculate
		- {cron: '* * * * *', callback: [@stats, calculate]}
		
		# calling static method of a class
		- {cron: '*/2 * * * *', callback: App\Model\Monitor::echo}
		
		# referencing jobs already registered as services
		- @scheduledJob
		- @App\Model\OtherScheduledJob
		
		# classes can be used directly - they will be registered into DI container automatically
		- App\MyOtherJobRegisteredIntoDIByScheduler
```

### Setup
The DI extension needs to be registered first:
```neon
extensions:
	scheduler: ElektroPotkan\Scheduler\DI\SchedulerExtension
```

To use more advanced schedulers than the `Simple` one,
You need to give the `path` to a directory,
where they can store their locks and timestamps.
```neon
scheduler:
	path: '%tempDir%/scheduler'
```

If You just set the `path` as in the example above, the `LastCheck` scheduler will be used.

You can control the exact used scheduler by the optional `lastX` option:
```neon
scheduler:
	lastX: no    # uses Locking scheduler
	lastX: check # uses LastCheck scheduler (the default one if the lastX option is missing)
	lastX: run   # uses LastRun scheduler
```

### Callback jobs
Register your callbacks under `scheduler.jobs` key.

```neon
services:
	stats: App\Model\Stats

scheduler:
	jobs:
		# stats must be registered as a service and have method calculate
		- { cron: '* * * * *', callback: [ @stats, calculate ] }
		
		# Monitor is class with static method echo
		- { cron: '*/2 * * * *', callback: App\Model\Monitor::echo }
```

Be careful with cron syntax, take a look at following example.
You can also validate your cron using [crontab.guru](https://crontab.guru).

```
  *  *  *  *  *
  -  -  -  -  -
  |  |  |  |  |
  |  |  |  |  |
  |  |  |  |  +----- day of week (0 - 7) (Sunday=0 or 7)
  |  |  |  +---------- month (1 - 12)
  |  |  +--------------- day of month (1 - 31)
  |  +-------------------- hour (0 - 23)
  +------------------------- min (0 - 59)
```

### Custom jobs
For classes that implements the `ElektroPotkan\Scheduler\IJob` interface.

Register your class into config directly as jobs.

```neon
scheduler:
	jobs:
		- App\Model\ScheduledJob
		- App\Model\OtherScheduledJob
```

You can also reference already registered service.

```neon
services:
	scheduledJob: App\Model\ScheduledJob
	- App\Model\OtherScheduledJob

scheduler:
	jobs:
		- @scheduledJob
		- @App\Model\OtherScheduledJob
```


Authors
-------
- Modified fork by Elektro-potkan <git@elektro-potkan.cz>.
- Original Contributte package authors (see [Composer config file](composer.json)):
  - [Milan Felix Šulc](https://f3l1x.io)
  - [Josef Benjac](http://josefbenjac.com)


Info
----
### Versioning
This project uses [Semantic Versioning 2.0.0 (semver.org)](https://semver.org).

### Branching
This project uses slightly modified Git-Flow Workflow and Branching Model:
- https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow
- https://nvie.com/posts/a-successful-git-branching-model/


License
-------
You may use this program under the terms of the MIT License.

See file [LICENSE](LICENSE).
