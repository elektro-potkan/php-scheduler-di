.PHONY: clean clean-tests-temp phpstan tests coverage-html coverage-xml


TMPDIR=/tmp/php-packages-tests/elektro-potkan/scheduler-di

TMPDIR_TESTS=$(TMPDIR)/temp


all: phpstan tests

clean:
	-rm tests/phpstan.local.neon
	-rm -r '$(TMPDIR)'

clean-tests-temp:
	-rm -r '$(TMPDIR_TESTS)'


$(TMPDIR):
	mkdir -p '$@'

tests/phpstan.local.neon:
	echo 'includes:' > '$@'
	echo '	- phpstan.neon' >> '$@'
	echo 'parameters:' >> '$@'
	echo '	tmpDir: $(TMPDIR)/phpstan' >> '$@'


phpstan: $(TMPDIR) tests/phpstan.local.neon
	vendor/bin/phpstan analyse -c tests/phpstan.local.neon -l max src

tests: $(TMPDIR) clean-tests-temp
	vendor/bin/tester -s -p php -c tests/php.ini --temp '$(TMPDIR)' tests/cases

coverage-html: $(TMPDIR) clean-tests-temp
	vendor/bin/tester -s -p phpdbg -c tests/php.ini --temp '$(TMPDIR)' --coverage '$(TMPDIR)/coverage.html' --coverage-src src tests/cases

coverage-xml: $(TMPDIR) clean-tests-temp
	vendor/bin/tester -s -p phpdbg -c tests/php.ini --temp '$(TMPDIR)' --coverage '$(TMPDIR)/coverage.xml' --coverage-src src tests/cases
